//Maybe but that in the theme ?

interface Breakpoints {
    s: string
    sm: string
    m: string
    l: string
    xl: string
}

const bp: Breakpoints = {
    s: "576px",
    sm: "768px",
    m: "992px",
    l: "1200px",
    xl: "2000px"
}

export default bp