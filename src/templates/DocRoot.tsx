import React from 'react'
import { graphql } from "gatsby"
import styled from 'styled-components'
import bp from "../breakpoints"
import { Link } from "gatsby"
import DocCard from "../components/DocCard"
import SEO from '../components/seo'

const StyledPostContent = styled.div`
max-width: 71%;
& a {
  text-decoration: underline;
    color: hsl(0, 1%, 1%);
}
`

const DocRoot = ({data: {markdownRemark}}) => {
    return (
      <React.Fragment>
        <SEO Title={markdownRemark.frontmatter.title} Description={markdownRemark.frontmatter.description}/>
        <StyledPostContent dangerouslySetInnerHTML={{ __html: markdownRemark.html }}/>
          {markdownRemark.children.map((doc, index) => <DocCard key={index} doc={doc}/>)}
      </React.Fragment>
    )
}
 
export default DocRoot

export const query = graphql`
  query($slug: String!) {

    markdownRemark(fields: { slug: { eq: $slug } }) {
      children {
        ... on MarkdownRemark {
          id
          fields {
            slug
          }
          frontmatter {
            title
            description
            tags
          }
          children {
            ... on MarkdownRemark {
              id
              fields {
                slug
              }
              frontmatter {
                title
                tags
              }
              children {
                ... on MarkdownRemark {
                  id
                  frontmatter {
                    title
                    tags
                  }
                  fields {
                    slug
                  }
                }
              }
            }
          }
        }
      }
      html
      frontmatter {
        title
      }
    }
  }
`