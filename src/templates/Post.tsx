import React from 'react'
import { graphql } from "gatsby"
import styled from 'styled-components'
import bp from "../breakpoints"
import SEO from '../components/seo'

const StyledPostContent = styled.div`
padding-top: 2em;
max-width: 93%;
@media (min-width: ${bp.m}) {
  max-width: 65%;
}
@media (min-width: ${bp.m}) {
  max-width: 52%;
}
& a {
  text-decoration: underline;
    color: hsl(0, 1%, 1%);
}
`

const Container = styled.div`
display: flex;
flex-direction: column;
`

const FooterImg = styled.img`
  height: 6em;
  margin-top: auto;
`


const Post = ({data: {markdownRemark : {html, frontmatter}}}) => {
    return (
      <React.Fragment>
        <SEO Title={frontmatter.title} Description={frontmatter.description}/>
        <StyledPostContent dangerouslySetInnerHTML={{ __html: html }}/>
        {frontmatter.footer_img.publicURL && <FooterImg src={frontmatter.footer_img.publicURL}/>}
      </React.Fragment>
    )
}
 
export default Post

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        footer_img {
          publicURL
        }
      }
    }
  }
`