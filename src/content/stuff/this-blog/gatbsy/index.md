---
templateKey: Doc
title: Gatsby - React
description: "Technical ressources about this website."
tags: ["technical", "javascript", "web"]
---

Here are some notes I took when building this website.

* [Debugging the gatsby build process](https://github.com/gatsbyjs/gatsby/blob/master/docs/docs/debugging-the-build-process.md#vs-code-debugger-auto-config)

* [React spring vizualizer](https://react-spring-visualizer.com/)