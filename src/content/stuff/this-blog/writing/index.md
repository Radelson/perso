---
templateKey: DocRoot
title: Writing
description: "Some links I found. Could keep me motivated to come back here and update this site."
tags: ["writing", "repository"]
---

# On writing

[Why I Keep a Research Blog](https://news.ycombinator.com/item?id=22033792)

## Inspirations

https://blog.burntsushi.net/foss/
https://news.ycombinator.com/item?id=22156868
http://0pointer.net/blog/
https://joelhooks.com/digital-garden
https://0xc45.com/
https://tomcritchlow.com/blogchains/digital-gardens/
https://news.ycombinator.com/item?id=23911577

## Ideas

- Drupal ajax widget
- FOSDEM
- React + Drupal ajax commands reactivity