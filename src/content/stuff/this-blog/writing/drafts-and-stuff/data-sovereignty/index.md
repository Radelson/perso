---
templateKey: Doc
title: On why I'm hosting this at home
modified_time: 18/04/2020
img: 74290225_2386816368300577_4886721786894352384_n.jpg
tags: ["Personal"]
description: "I was thinking of why I felt such a need to self-host this website and other services, and I felt compelled to write it down. That should be interesting if I stop one day or something.
"
---

# On why I'm hosting this at home and other time waste

I feel I could go on and on about it forever. I'm myself wondering how writing all this will end.
I think I should start with what it stems from.

## I don't feel nice using most SaaS

Multiple reasons at that. Without much surprise, I'm (was ?) a big user of everything google. At first it felt nice.
Gmail, Gmap, Drive, those services felt like they were built with users in mind and I've only nice memories of those early days. Welp, that changed and now all those services are just ingesting my data while only providing an Okay experience tainted with advertising.

I don't feel like beating a dead horse and I won't go over everything that is wrong with mega corps and their services. I think the interesting thing for me is that it stopped feeling acceptable using those services. I kept cynically using them, all the while saying to anybody listening how everything was wrong about them.

## This is gardening for geeks

Eh, you heard it before about blogs, but I think it still holds.

## Data sovereignty feels like an act rebellion

Really, it feels great.

## A wood worker isn't satisfied coming back from a trip to ikea

Could have found a more apt comparison but anyway. What I wanted to say is that I'm doing stuff professionaly in this area of expertise and it doesn't feel right being lazy about it when it's for me. I can understand that not everyone should self host, but I think it's definitely right up my alley to do it.