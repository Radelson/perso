---
templateKey: Doc
title: Is this thing on ?
modified_time: 13/01/2020
img: 74290225_2386816368300577_4886721786894352384_n.jpg
tags: ["Personal"]
description: "This post should define my goals and motivations for starting this blog/website. It should make for a good read several years down the line, if only as a reminder for myself."
---

# This blog inception

This post should define my goals and motivations for starting this blog/website. It should make for a good read several years down the line, if only as a reminder for myself. Also, I guess I should set an editorial line for the content I'm going to (hopefully regularly but don't hold your breath) post here.

I don't know yet the form these posts should take. I find myself liking to read text with well defined sub-parts I can easily skim through, I think that's what I'll do. I'll start talking about the motivations behind this site and then I'll talk about the content I expect to post. We'll see where it goes from there.

## I want to delete my social network accounts

This is something I want to do since a long time. I've always felt some cognitive dissonance about that. I could speak to length about what is wrong about social networks but, hey, we all know. The reasons why I didn't walk the talk till now are because of my unsecurities I guess ? It feels jarring deleting the only place where anyone could contact me even if not knowing my phone number. I mean, I'm already pretty disconnected, that last step feels like pulling the plug on my social life artificial respirator. Now if I am actually pulling that plug, I can at least redirect everyone to my email and phone number on this website and calm my Fear Of Missing Out™. eh

## I want to control what you're able to find when looking for my name

I just did the experiment and there is (1) my linkedin profile closely followed by (2) my facebook profile. If you're typing my name in that search box, you most certainly want to know what I'm about and neither of the two profiles are worth anything for that.

## I want to produce more

This realization came a bit late for me (it seems to come naturally for some people ?) but consuming too much without producing anything is seriously mind numbing. Writing is not a skill I have, it's no something I do. But I think writing can be a nice and convenient way to get myself to produce more, even if it's only for the sake of producing something. It feels good.

## I want to express myself

I've never been a social network guy. The things I want to talk about I feel most of my social network don't care. Sometimes I've felt the urge to write something but I was too uncomfortable to do it when I knew most of my friends/family scrolling through their feeds would have to see my 3am ramblings about nerdy stuff. But still, I wanted to be able to express myself on some topics. On this medium, I know my audience won't be the same and I feel at ease writing this here.

## I want to be what I admire ?

This one I had a hard time laying it out but bear with me.
I feel everyone has their own comsumptions habits that they eventually want to become an actor of. If you enjoy reading books, eventually you'll contemplate trying your hand at writing one, same for music et al.

The main things I consume these days are 1) vidya gaymes 2) blog posts and stuff lying around on the internet that someone else put there. Developing games, that ship has sailed a long time ago, that's not what I aspire to. But yeah, I want to put my stuff around on the internet for someone else to find. It could have been worse for an unrealized dream.

## The content and audience for it

I think my main audience will be the google web crawlers. I should fully embrace that and go wild, I don't expect any hooman to come here and spoil my fun. It's quite liberating. If you're reading this through a screen, well, let's hope you'll be as civil as the web crawlers and don't judge.

The reader I'll keep in mind when writing is my future self. I'm not so keen on keeping physical contraptions or pics as souvenirs but it'll be fun to read my thoughts in a couple of years and cringe at the misconceptions I had (have ?). I don't want this website to be too intimate though. I'll keep it to strictly public things and I'll never post something that I don't want to others to read. The fact that I'll be my own audience in the future is only a byproduct.

Some posts will be technical writings and those I want to be easily discoverable from the outside. I don't think I have anything novel to say but I keep returning to some well crafted blog post as valuable ressources. I want to give back with some nice technical write up about some things I do (I guess web development, devops practices and more on that vein).

Another type of post 



## This is my main reading source these days

## 
## Toto munus Thebae lecti ferax fugae Avernales

Lorem *markdownum quoniam canentis* funera, moderator cum ambrosia Aglauros
vitam. Fama talia, currusque; verte ipso, ore plura materiam, in manu poteras
senectus. **Errat cepimus**, sed *quam*, et idque est fugientem est. De
succeditis et alto fortunam haerentem barbam quod illud pedibusque cretosaque
draconem! Conclamat redeunt *postquam disiectum frustra* semper magis, atque
lascivitque vera animis nec dependent curras!

    if (source < ics_protector_software) {
        rateDvd.text(flops_dual_pad(cardClone));
        fios -= pngStorage;
        streamingMnemonicInterface = media_mca(2, xhtml_pcmcia);
    } else {
        token_plagiarism.pseudocode(phreakingSector);
        mtuDirectPoint(791035);
        display_in.esports_daw(wrapFriend);
    }
    var graphicFlowchartToken = -2;
    if (-2 != dvIo / matrixPaper) {
        web.crop.recycle_dns_mpeg(server, wampPodcastCrt);
    } else {
        diskHalftonePci(readme, cdma_mode, binary);
        smart(sound_file, index * 28);
    }

## Dumque cernitis custodit

Hanc teneri, Melampus datis! Mitem vellet Cephesidas nulla procul annos longo,
qua! Ira corruat carpitur super. Traduxit putares
[qua](http://aratri-tamen.net/saevitiae) prima referat Lycaon
[crinem](http://quasque-pro.io/habetsprevere.aspx), qui di ira hanc, onerosior,
tune! Poterit coeptum tertius tradat, haec arma in tinnitibus cuspide abiit.

- Nostris membra nemus et te rastra tamen
- Captum regemque quamquam dolore tu nigra postera
- Latet rure Deianira

## Et iacent dea humani Tritonida cunctos castos

Confodit et respondet tollensque sulphura fractarum quicquid placida et fixerat
animal summoque. Fallacis nidi veniente non solo haec magne iuvenum, hoc
feroxque molles.

1. Nec ratem gravem si attolle terras
2. Non sibi
3. Flavusque Athenas inploravere
4. Pulmonis in vivaci silvis rutilis

[Lacrimis](http://www.tunc.org/) liquefaciunt, falsa nulla ramis cum, in est;
sub sanguine porrexit certe putares quid
[patris](http://operitur-telaque.org/commissaque) tactas dum. Pervenit ni nunc
virgo loca, defodit omne aut conpressit corpore, cum successurumque passibus.
