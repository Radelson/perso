---
templateKey: DocRoot
title: This blog
description: "Ressources related to this website. May be technical or not ? Probably this will become a big mess."
tags: ["meta", "thoughts", 'repository']
---
# Links

[Minimalist web design HN thread](https://news.ycombinator.com/item?id=22053598)
Nice thread on web perf : https://news.ycombinator.com/item?id=22288675
Nice thread on SVG animations : https://news.ycombinator.com/item?id=22297461