---
templateKey: Doc
title: Nfs shares
description: "Notes about the setup for accessing NFS shares from outside home"
tags: ["???"]
---
# NFS

Nextcloud wasn't good enough for my clients (erm, family). They wanted it mounted as a real fs. I had already setup NFS and wireguard so I thought why not provide some NFS shares for them.

## Wireguard

### Server
Setup was a bit complicated because it's running in a container. I 