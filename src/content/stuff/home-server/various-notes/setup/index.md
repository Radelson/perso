---
templateKey: Doc
title: Homeserver setup
description: "The server setup. Basically everything I did after assembling. Those are raw notes."
tags: ["???"]
---
# Server setup

Never mind the assembly and archlinux setup.

## TODO

* review fw before opening ports in router ?
* review consommation
  * undervolt processor
  * stress test
* run a memtest
* review kernel options for perf/btrfs/etc
  * review mount options for FS
* setup btrfs
  * Check the possibility of having a nocow subvolume for dbs VS hosting dbs on SSD
* setup k3s
  * Mostly done but I'd like some logs/audit services ?

### DONE

* Base system setup
* Basic BTRFS setup
* DYNDNS setup
* Port forward setup

## Setup

### Arch install

I did the regular install from the [wiki](https://wiki.archlinux.org/index.php/installation_guide#Pre-installation). 

#### Some of the steps not specified

- I had some trouble getting my terminal to work fine via ssh.
Urxvt + Kitty were equally unusable.
It was solved with `kitty +kitten ssh root@******`

- I decided to try using XFS for my / partition because why not. I'm going to fiddle with everything for days anyway. According to [this](https://access.redhat.com/articles/3129891#choosing-a-local-file-system-6) it could have an edge vs ext4 for large bandwidth and a lot of parralels IO. Could be my case. We'll see. I expect the tools to be nice too.

- Installed a bunch more packages + enabled dhcpd and opensshh


### Basic setup

Here is the log of what I did next

- Securing access for the root account + setup an user for me to use
- Setup my ssh key + the usual sshd config
- Setup zsh + oh my zsh
  - Not a fan of oh my zsh but hey, it's easy ? Should look for a replacement
- Setup networking -- static addr = *192.168.0.100*
  - Decided on using systemd-networkd in place of the usual netctl. It looks pretty nifty. Let's embrace the god almighty systemd.
  - [Doc is here](https://wiki.archlinux.org/index.php/Systemd-networkd)
  - Removed netctl and dhcpcd

### Storage Setup

#### BTRFS

For now I have 1 To in raid 1 using BTRFS.
Ressources used :
- https://btrfs.wiki.kernel.org/index.php/SysadminGuide
- https://seravo.fi/2016/perfect-btrfs-setup-for-a-server
- https://btrfs.wiki.kernel.org/index.php/Using_Btrfs_with_Multiple_Devices
- https://wiki.archlinux.org/index.php/Btrfs#File_system_on_a_single_device
- https://forum.manjaro.org/t/btrfs-tips-and-tricks/71186 -> Standard mount options recommendations
- https://news.ycombinator.com/item?id=22159204

<del>I still have to do the mount points/devise a sub volumes strategy maybe ?</del>
<del> I mount everything on /mnt/disks.</del>

Yeah no, we have subvolumes now. It's really neat not having to think about partitions and it seems easy to have snapshots for differents things like that.

##### Interesting stuff on BTRFS
- Written by a maintainer working at Suse. cool guy
 https://github.com/kdave/btrfsmaintenance 

#### NFS

At first I wanted to have my k3s workloads bound to a specific node and be done with it. But really, it's ugly. I want to be able to decouple the loads from a specific environment if eventually I get a frankencluster running. 

CEPHFS and glusterfs are neat and could enable stripping between nodes (and that's exciting to me) but the thing is already overly complex rn.

##### Ressources

- https://wiki.archlinux.org/index.php/NFS

### DNS

Duckdns looks like a fair provider and I didn't notice any other that didn't look like it was going to spy on me. So duckdns it is.

My registrar is already giving me a free ssl cert and I like them (porkbun) so I stayed with them. Here is the simple setup.

 *.adel.cool CNAME -> adelson.duckns.org A -> k8s ingress