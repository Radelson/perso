---
templateKey: Doc
title: Homeserver
description: "Ressources about my personal homeserver. This should be technical documentations for myself when I need to work on it after a few months."
tags: ["technical", 'repository']
---
# Homeserver

This is the file used for gathering informations on my homeserver project.

## Specs

### Requirements

- Cheap
- Not an electricity hog
- Should tend to be rebuildable automatically (config as code)
- Should be able to reuse some of my parts lying around
- Be reliable
- Exciting
- Learnings should be applicable at my day job

### Parts lying around

| Part                            | Numbers       |Reused |Comment|
| --------------------------------|---------------|-------|---|
| rackmounted server chassis      | 3             | 1     |bulky but nice|
| trashy laptops                  | ? a lot       | phase2|could be clustered but probably not worth the hassle, only for learning|
| HDDs                            |5 amounting 8to| all   | need to have a reliable way to create heterogenous raids for them|
| old power hungry quad core      |1              | hopefully not |could be clustered but too power hungry
|salvaged PSU 650w| 1 | 1 | very nice antec semi modular PSU



### Storage

I was a total noob when I started gathering informations. So everything in this section should be taken with a grain of salt. This is just from reading some online informations and not really clear in my mind.

#### Btrfs vs ZFS vs ??

Btrfs won
 1) it's mainlined 
 2) it looks stable to me (gut check from internet reading) despite its history
 3) keeps me on my toe and I'll have backups anyway ;)