---
templateKey: Doc
title: K3S
description: "Notes on K3S setup"
tags: ["k3s"]
---
# K3S setup

## TODO

* Read on how to do authentication/manage accounts
* Check what make sense commiting (ressources definitions)
* Redo storage using plain nfs for plain data

### DONE

* Read more docs so I get acquainted with the linguo
* Basic setup
* Dashboard setup
* Local static storage setup
* Repository for definitions

## Setup

I pretty much just followed the 'Getting started guide'
I was reticent just running the script but it looked more or less straightforward (creating the systemd unit definition) and not doing too much magic ?

I did :
- Setup the k3s context on my local kubeconfig
- Setup the kubernetes dashboard but couldn't make the login work with my kubeconfig, had to create a service account and login with it's token.

### Storage

<del>
- https://github.com/kubernetes-sigs/sig-storage-local-static-provisioner/blob/master/docs/operations.md#use-a-whole-disk-as-a-filesystem-pv
- https://kubernetes.io/blog/2018/04/13/local-persistent-volumes-beta/
? What the clean job option ?
</del>


Scratch that we doing NFS now.

https://github.com/helm/charts/tree/master/stable/nfs-client-provisioner

Some guys come back from using it here : https://news.ycombinator.com/item?id=23302871 ctrl-f nfs

#### Dream storage setup

Currently I have 2 storage classes. Host path and NFS. NFS is slow af, it's probably good enough for files but it's not good for the rest.

Ideally I'd like.

- A fast storage class providing block storage over the network (sharing an SSD)?
- Nfs-raid1 storage class providing a shared FS to be accessed by multiple services (like torrent+owncloud on the same data e.g.)
- Something funny like pooling unused space from all the nodes -> that could be rook or longhorn ?