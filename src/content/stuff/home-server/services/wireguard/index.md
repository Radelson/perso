---
templateKey: Doc
title: WireGuard
description: "Yes."
tags: ["Security", "VPN"]
---
# Wireguard

It'll be useful to let only NFS, K8S, etc accessible from the LAN.

Straight up followed this : https://blog.levine.sh/14058/wireguard-on-k8s-road-warrior-style-vpn-server
https://miguelmota.com/blog/getting-started-with-wireguard/
+ Archlinux wiki to understand better, as always.


## Setup

Setup was mindlessly following the Levine's blog post.
Only thing I had to do is reboot the computer + setup port forwarding on my router.
All in all, pretty easy.

Just so I don't feel dumb next time, here is the command for the tunnel.
```sudo wg-quick up wg0```

## TODOS

https://github.com/NixOS/nixpkgs/issues/30459#issuecomment-495083501

## Logs

### 02-08-20

- Well, I can access my lan from outside. Exciting time indeed.

## 13-08-20

- Wireguard is only accepting traffic going to the server and nothing else, that's way better for me at the time. Probably later I can setup multiple connections so I still run everything through it if I so want.
