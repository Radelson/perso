---
templateKey: Doc
title: NFS
description: "Some notes about NFS. I thought it was EZ but there is definitely more to it than I thought."
tags: ["Storage"]
---
# NFS

## Setup

Setup was straightforward.

Install it.
Edit /etc/exports following instructions from man or any random post.
Here is the current setup 02/08
```
/srv/nfs/k8s    192.168.0.0/24(rw,sync,no_root_squash)
/srv/nfs/music    192.168.0.0/24(rw,sync,all_squash,anonuid=33,anongid=33,no_acl)
/srv/nfs/nextcloud-data    192.168.0.0/24(rw,sync,all_squash,anonuid=33,anongid=33,no_acl)
```

## Issues

### Permissions

That where it gets hairy. The basic stuff is easy but then I hit some permissions issues.
All_squash was supposed to map every access to the same user (http:http). I figure it's easier than messing around trying other solutions (idmapping ? having the same groups across hosts but then you have to setup things inside containers too..). It didn't work for a while (the uid/gid were still the one from the remote writer) until I tried adding no_acl -> it works fine now.

### Mounting fails at boot

https://unix.stackexchange.com/questions/486777/etc-fstab-fails-to-bind-mount-on-boot-but-running-mount-a-works-correctly
http://codingberg.com/linux/systemd_when_to_use_netdev_mount_option ---> that mount option is straight up fire

## TODOS

## Logs

### 02-08-20

- Added the magical "no_acl" option and it finally works smoothly.
