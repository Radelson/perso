---
templateKey: Doc
title: Gitlab runner
description: "The gitlab runner instance setup doc"
tags: ["???"]
---
# Gitlab runner

It was nicer to have the ci/cd runner directly embedded in the cluster so we don't have to handle authenticating against the cluster for deploying.

## Setup

Used the helm chart here. [Helm chart](https://docs.gitlab.com/runner/install/kubernetes.html)

Had to fiddle a little bit at first because I'm using DinD in the pipeline and it warrants some env variables when running in kubernetes :

```
variables:
  DOCKER_HOST: tcp://localhost:2375
  DOCKER_TLS_CERTDIR: ""
```

Also, the RBAC setup didn't work, had to check this issue : https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3841

## Interesting stuff

No interesting stuff this time. This was ez and straightforward.

## Admin Logs

### 18-04-20

- Got it running on the cluster.
