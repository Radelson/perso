---
templateKey: Doc
title: Services
description: "Some notes about the services I want to host."
tags: ["???"]
---
# Services

## What I currently host

- This website
- Nextcloud instance

## I want to host

- ~~Some kind of log analyzer/website tracking~~ Ackee
- Home automation software. WebThing or HA ?
- Prometheus instance
- Mail server ?


## Links

https://github.com/awesome-selfhosted/awesome-selfhosted