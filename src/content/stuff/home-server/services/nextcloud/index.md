---
templateKey: Doc
title: Nextcloud
description: "Doc about the nextcloud instance"
tags: ["???"]
---
# Nextcloud

This is pretty great. Replace the google suite and more.

## Setup

Used the helm chart here. [Helm chart](https://github.com/helm/charts/tree/master/stable/nextcloud)

That was my first Helm experience. It's ok.

The values used are in my K8S repo.

Eventually it'd be nice to fork it.

1) I d'like to have different locations for /data and the php code base/conf because rn everything is on a NFS share and it's dog slow.
2) Can't setup a startup probe. Workaround was setting the other liveness probe delay very high. (startup is slow af and the pods was restarted infinitely, because of 1)
3) Only HA redis and HA mysql. I don't think I need that.

### Update

It's not clear how to update. I fear I'm going to bork it at the first update.

Apparently the docker entrypoint has a script trying to update automatically.

### Backup

I don't know yet how to do the db backups. The files could be handled by BTRFS or Velero ?

## Errors and how to fix them

### Couldn't chown over NFS

Without that export option, the install scripts wouldn't run.
Export option no_root_squash

### getLog not found

A bit weird and I don't explain it totally. The error was only after powercycling the server.
When trying to access nextcloud I had this error ```PHP Fatal error: Uncaught Error: Call to a member function getLogger() on null in /var/www/nextcloud/index.php:72```

When trying to use the cli here is what I got.

```
#6 {main}root@nextcloud-645f94b795-fhkjm:/var/www/html# runuser --user www-data -- php occ
An unhandled exception has been thrown:
Exception: Could not acquire a shared lock on the config file /var/www/html/config/config.php in /var/www/html/lib/private/Config.php:211
Stack trace:
#0 /var/www/html/lib/private/Config.php(64): OC\Config->readData()
#1 /var/www/html/lib/base.php(145): OC\Config->__construct('/var/www/html/c...')
#2 /var/www/html/lib/base.php(582): OC::initPaths()
#3 /var/www/html/lib/base.php(1089): OC::init()
#4 /var/www/html/console.php(48): require_once('/var/www/html/l...')
#5 /var/www/html/occ(11): require_once('/var/www/html/c...')
#6 {main}root@nextcloud-645f94b795-fhkjm:/var/www/html# cat /var/www/html/lib/private/Config.exit
command terminated with exit code 1
```

Eventually I saw that flock() could be flaky when used over NFS. I customised the deployment to only use NFS for the /data directory and we're good now.

### Interesting stuff

- occ config:list system -> shows the loaded config


## TODOS

- Put the config files into a secret, do not serve it from the FS.
That could be hard because it's writable by the nextcloud instance. (When it's updating e.g.)
- Review how to setup onlyoffice.

## Logs

### 18-04-20

- Added files from an external source and ran occ file:scan --all
It works ok but I had trouble after that deleting what was added this way -> Sabredav Error file locked
I had to disable the filelocking mechanisme in config.php (fileLocking = false) I couldn't find out why.
I'm not too worried about it because NFS is implementing a lock on files too as I previously found out (see getLog not found)

- Created some basic groups and added shared movies/music. That was nice.

## +-24-08-20

Welp, I think I forgot about this log thingy, looks interesting.

- Updated NC, to the latest version. It went fine.

- I realised it wasn't doable to have some directories I intended to be shared with other services (music stuff, documents, etc) managed by NC so I need to use the external storage app for them. It'll be easier to reuse and backup too if I hav different shares

### 01-08-20

- Installed a bunch of apps just to see what shticks

- Finished the work on the shared directories (setup nfs shares namely)
