---
templateKey: Post
title: Is this thing on ?
modified_time: 20/01/2020
img: 74290225_2386816368300577_4886721786894352384_n.jpg
tags: ["Personal"]
description: "This is me finally embracing my digital self."
footer_img: boots-03.svg
---

# This blog inception


I initially prepared an overloaded text where I wrote about every reason I had for building this site but I scrapped that eventually.

Let's keep it at the bare minimum.

I intend to post some technical stuff about things I had fun doing and that I want to share.

There is a "stuff" section, that's for me blurping things on my keyboard (yeah, not for human consumption).

The blog will be on gitlab somewhere once the code is cleaned up, I learned a lot along the way and I want to have everything up to par.

We'll see how it goes.