import bp from "../breakpoints"

export function mergePostsImgs(post, deskPost) {
    return {
        ...post,
        imgs: [
            post.node.frontmatter.img.childImageSharp.fluid,
            {
              ...deskPost.node.frontmatter.img.childImageSharp.fluid,
              media: `(min-width: ${bp.m})`,
            },
          ]
    }
}