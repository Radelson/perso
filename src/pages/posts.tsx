import React from "react"
import { graphql } from "gatsby"
import SEO from "../components/seo"
import { MarkdownRemarkEdge } from "../graphqlTypes"
import PostCard from "../components/PostCard"
import { mergePostsImgs } from "../helpers/Helpers"
import styled from "styled-components"

// Wtf am i even doing here. Let's read more on generics

const StyledH1 = styled.h1`
`

const PostsPage: React.FunctionComponent<{data}> = ({data: {AllImgsDesk, AllPosts}}) => {
  const AllPostsMassaged = AllPosts.edges.map((edge, index) => (mergePostsImgs(edge, AllImgsDesk.edges[index])))
  return (
    <React.Fragment>
      <SEO Title="Posts"/>
      <StyledH1>Posts</StyledH1>
        {AllPostsMassaged.map((post, index) => <PostCard key={index} post={post}/>)}
    </React.Fragment>
  )
}

export const pageQuery = graphql`
  query {
    AllPosts : allMarkdownRemark(
      sort: {order: DESC, fields: frontmatter___modified_time},
      filter: { frontmatter: { templateKey: { eq: "Post" } } }
    ) {
      edges {
        node {
          fields {
            slug
          }
          html
          frontmatter {
            tags
            img {
              childImageSharp {
                fluid(maxWidth: 600, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
            templateKey
            title
            description
          }
          parent {
            ... on File {
              modifiedTime(formatString: "DD/MM/YYYY")
            }
          }
        }
      }
    }
    AllImgsDesk : allMarkdownRemark(
      filter: { frontmatter: { templateKey: { eq: "Post" } } }
    ) {
      edges {
        node {
          frontmatter {
            img {
              childImageSharp {
                fluid(maxWidth: 1400, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`
export default PostsPage
