import React from "react"
import { Link, graphql } from "gatsby"
import bp from "../breakpoints"
import SEO from "../components/seo"
import styled from "styled-components"
import Obfuscate from "react-obfuscate";
import IconPhone from '../images/phone.svg'
import IconMail from '../images/mail.svg'

const PhoneContainer = styled(IconPhone)`
  height: 6em;
  margin: auto;
  @media (min-width: ${bp.xl}) {
    height: 8em;
}

& .st0 {
  stroke:${props => props.theme.colors.secondary};
}
`
const MailContainer = styled(IconMail)`
  height: 6em;
  margin: auto;
  @media (min-width: ${bp.xl}) {
    height: 8em;
}
& .st0 {
  stroke:${props => props.theme.colors.secondary};
}
`
const IndexPageContainer = styled.div`
  display: flex;
  font-size: 1.4em;
  height: 100%;
  flex-direction: column;
  align-content: space-between;
`
const IntroContainer = styled.div`
  font-weight: 250;
  text-align: center;
  margin: auto;
  padding: 0 1em 0 1em;
  @media (min-width: ${bp.sm}) {
    padding: 0;
}
  & a {
    text-decoration: underline;
      color: hsl(0, 1%, 1%);
  }
  & p {
    margin: 0.5em 0 0.5em 0;
  }
}
`
const StyledObfuscate = styled(Obfuscate)`
  margin: auto;
`

const LinksContainer = styled.div`
  margin: auto 0 2em 0;
  display: flex;
  flex-direction:row;
  align-content: space-between;
`

const IndexPage: React.FunctionComponent = () => {
  return (
    <React.Fragment>
      <SEO Title="Home"/>
      <IndexPageContainer>
        <IntroContainer>
          <p>Welcome to my virtual place.</p>
          <p>It's still really empty but we'll get there eventually.</p>{/*  Hopefully I'll come back here regularly and add stuff.</p> */}
          <p>Maybe you'd like to read <Link to="/posts/">my last ramblings</Link> or perhaps know more <Link to="/about/">about me</Link>.</p>
        </IntroContainer>
        <LinksContainer>
          <StyledObfuscate tel='+32486217154' ><PhoneContainer/></StyledObfuscate>
          <StyledObfuscate email="me@adel.cool"><MailContainer/></StyledObfuscate>
        </LinksContainer>{/*Gitlab, Drupal.org, Mastodon ?*/}
      </IndexPageContainer>
    </React.Fragment>
  )
}
export default IndexPage
