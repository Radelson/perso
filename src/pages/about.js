import React from "react"
import SEO from "../components/seo"
import styled from "styled-components"
import bp from "../breakpoints"
import cv from "../files/cv.pdf"

const AboutContainer = styled.div`
padding-top: 2em;
max-width: 93%;
text-align: left;
& header {
  font-weight: 500;
  padding: 1em auto auto auto;
  text-align: center;
}
@media (min-width: ${bp.m}) {
  max-width: 60%;
}
& a {
  text-decoration: underline;
    color: hsl(0, 1%, 1%);
}`

const About = () => (
  <React.Fragment>
  <SEO Title="About" />
    <AboutContainer>
      <header>
        <h2>Me</h2>
        <p>(Adelson Ruelle)</p>
      </header>
      <section>
        <p>I'm currently the tech lead for a <a href="https://www.webstanz.be/" target="_blank">Drupal agency</a> in Belgium. I'm interested in free software, the web and a whole range of other stuff.</p>
        <p>Please get in touch if you got here.<br/> Contact infos on the home page (yeah, click on the illustrations).</p>
        <p>Here is my <a target="_blank" href={cv}>CV</a> too.</p>
      </section>
      <header>
      <h2>This site</h2>
      </header>
      <section><p>Yeah, what's the point ?</p><p>I dunno yet either.</p><p>Important stuff about all this : have fun and perfect is not required.</p></section>
    </AboutContainer>
  </React.Fragment>
)

export default About
