import React, { ReactNode, useEffect, useContext } from "react"
import styled, { ThemeContext } from "styled-components"
import { useInView } from 'react-intersection-observer'
import { useSpring, animated, config } from 'react-spring'

const StyledFooter = styled(animated.footer)`
  display: flex;
  flex-direction: column-reverse;
  align-items: center;
  font-size: 0.4em;
  font-weight: 100;
  font-family: ${props => props.theme.altFont}, monospace;
  padding-bottom: 0.5em;
  grid-area: footer;
  & a {
    text-decoration: underline;
      color: hsl(0, 1%, 1%);
  }
  text-align: center;
`

const Footer = (): ReactNode => {
  const [ref, inView] = useInView({
    rootMargin: '-100px 0px',
    triggerOnce: true,
  });
  const theme = useContext(ThemeContext);
  const [backDropAnimation, setBackDropAnimation] = useSpring(() => ({config: config.molasses, from: {background: `linear-gradient(180deg, ${theme.colors.backgroundTransparent} 50%, ${theme.colors.background} 95%)`}}));
  useEffect(() => {
    if(inView) {
      setBackDropAnimation({background: `linear-gradient(180deg, ${theme.colors.backgroundTransparent} 50%, ${theme.colors.bleed} 90%)`});
    }
  }, [inView, theme]);

   return (
    <StyledFooter ref={ref} style={backDropAnimation}>
      <span>Using illustration by various artists created for <a href="https://www.autodraw.com/artists">the autodraw google project</a>.</span><span>Using <a href="https://github.com/ronotypo/Minipax">Minipax font from ronotypo</a> and <a href="https://github.com/Fonthausen/CrimsonPro">CrimsonPro from Fonthausen</a>.</span>
    </StyledFooter> 
  );
}

export default Footer;
