/**
 * Layout component
 */

 //@ See : https://css-tricks.com/a-dark-mode-toggle-with-react-and-themeprovider/
 
import React, { useEffect, useState } from "react"
import styled, { ThemeProvider } from 'styled-components'
import { LightTheme, DarkTheme } from "./Theme"
import NavBar from "./Navbar"
import GlobalStyle from "./GlobalStyle"
import bp from "../breakpoints"
import Footer from './Footer'
import IconMoon from '../images/moon.svg'
import IconSun from '../images/sun.svg'
import { useSpring, animated } from "react-spring"


const useDarkMode = () => {
  const [theme, setTheme] = useState('light');
  const [componentMounted, setComponentMounted] = useState(false);
  const setMode = mode => {
    window.localStorage.setItem('theme', mode)
    setTheme(mode)
  };

  const toggleTheme = () => {
    if (theme === 'light') {
      setMode('dark')
    } else {
      setMode('light')
    }
  };

  useEffect(() => {
    const localTheme = window.localStorage.getItem('theme');
    window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches && !localTheme ?
      setMode('dark') :
      localTheme ?
        setTheme(localTheme) :
        setMode('light');
    setComponentMounted(true);
        // let react take care of dynamic styles
          document.body.classList.remove('dark');
  }, []);

  return [theme, toggleTheme, componentMounted]
};

const ThemeSwitcher = React.memo(styled.div`
  overflow: hidden;
  display: flex;
  flex-direction: column;
  width: 6em;
  height: 100%;
  & svg {
  height: 3.5em;
  margin-left: 1em;
}
@media (min-width: ${bp.m}) {
  height: 50vh;
    justify-content: flex-end;
    width: 100%;
    & svg {
  margin: auto;
  height: 6em;
}}
@media (min-width: ${bp.l}) {
  & svg {

height: 8em;
}
  }
`)
const MoonContainer = styled(animated(IconMoon))`
transform-origin: center;
display: flex;
& .face {
  stroke-width:7;
  stroke:${LightTheme.colors.text};
  stroke-linecap:round;
  stroke-miterlimit:10;
}
& .body {
  stroke:${DarkTheme.colors.bleed};
  fill:${DarkTheme.colors.text};
  stroke-width:7;
  stroke-linecap:round;
  stroke-miterlimit:10;
  }
`
const SunContainer = styled(animated(IconSun))`
transform-origin: center;
display: flex;
& .st0 {

  stroke:${LightTheme.colors.secondary};
  fill:${LightTheme.colors.secondary};

}
& .st1 {
  stroke:${LightTheme.colors.secondary};
  fill:${LightTheme.colors.secondary};
  stroke-width:7;
  stroke-linecap:round;
  stroke-miterlimit:10;

}
& .st2 {
  stroke:${LightTheme.colors.text};
  stroke-width:6;

  }
`

const StyledIconMoon = animated(styled(animated.div)`
transform-origin: top center;
@media (min-width: ${bp.m}) {
  transform-origin: left center;
}
`)
const StyledIconSun = animated(styled(animated.div)`
transform-origin: bottom center;
@media (min-width: ${bp.m}) {
  transform-origin: right center;
}
`)

const LayoutContainer = styled.div`
    min-height: 100%;
    display: grid;
    grid-template-rows: 3.7rem auto 4.3rem;
    grid-template-columns: auto;
    grid-template-areas:
    "navbar"
    "content"
    "footer";
    @media (min-width: ${bp.m}) {
      grid-template-rows: auto 4.3rem;
      grid-template-columns: auto 20vw;
      grid-template-areas:
      "content navbar"
      "footer navbar";
  }
  @media (min-width: ${bp.l}) {
      grid-template-columns: auto 15vw;
  }
`
const ContentContainer = styled.main`
    grid-area: content;
    display: flex;
    flex-direction: column;
    align-items: center;
    ${props => props.noOverflow ? "overflow: auto;" : ""}
`
const Layout: React.FunctionComponent = ({ children, pageType}) => {
  const [theme, toggleTheme, ready] = useDarkMode()
  const iconPos = useSpring({
    to : { deg: theme == 'dark' ? 3 : 0 },
    from: { deg : theme == 'dark' ? 3 : 0 },
    config: { mass: 2, tension: 100, friction: 36, clamp: true }
  });

  return (
    <ThemeProvider theme={ theme == 'dark' ? DarkTheme : LightTheme }>
      <LayoutContainer>
        <GlobalStyle/>
        {ready && <NavBar>
          <ThemeSwitcher>
            <StyledIconMoon  onClick={toggleTheme} style={{transform : iconPos.deg.interpolate((x) => `rotate(${theme == 'dark' ? 3-x : x-3}rad)`)}}><MoonContainer style={{transform : iconPos.deg.interpolate((x) => `rotate(${theme == 'dark' ? x+3 : 3-x}rad)`)}}/></StyledIconMoon>
            <StyledIconSun  onClick={toggleTheme} style={{transform : iconPos.deg.interpolate((x) => `translateY(-3em) rotate(${theme == 'dark' ? x : -x}rad)`)}}><SunContainer style={{transform : iconPos.deg.interpolate((x) => `rotate(${theme == 'dark' ? x+6 : 6-x}rad)`)}}/></StyledIconSun>
          </ThemeSwitcher>
        </NavBar>}
        <ContentContainer noOverflow={pageType == "post" ? false : true}>
          {children}
        </ContentContainer>
        {ready && <Footer/>}
      </LayoutContainer>
    </ThemeProvider>
  )
}

export default Layout
