/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import Helmet from "react-helmet"
import { useStaticQuery, graphql } from "gatsby"

interface SeoProps {
  Description?: String
  Lang?: String
  Meta?: Array<any>
  Title?: String
}

const SEO: React.FunctionComponent<SeoProps> = ({ Description = '', Lang = "en", Meta = [], Title }) => {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            author
          }
        }
      }
    `
  )

  const metaDescription = Description || site.siteMetadata.description

  return (
    <Helmet
      htmlAttributes={{
        Lang,
      }}
      title={Title}
      titleTemplate={`%s | ${site.siteMetadata.title}`}
      meta={[
        {
          name: `description`,
          content: metaDescription,
        },
        {
          property: `og:title`,
          content: Title,
        },
        {
          property: `og:description`,
          content: metaDescription,
        },
        {
          property: `og:type`,
          content: `website`,
        },
        {
          name: `twitter:card`,
          content: `summary`,
        },
        {
          name: `twitter:creator`,
          content: site.siteMetadata.author,
        },
        {
          name: `twitter:title`,
          content: Title,
        },
        {
          name: `twitter:description`,
          content: metaDescription,
        },
      ].concat(Meta)}
    />
  )
}

export default SEO
