import React, { useState, useRef, useEffect, useContext } from "react"
import { Link } from "gatsby"
import styled, { ThemeContext } from "styled-components"
import bp from "../breakpoints"
import { animated, useSpring } from "react-spring"
import { debounce } from 'lodash'
import { DarkTheme } from './Theme'


const Burger = styled.div`
  display: flex;
  padding: 0.7em;
  cursor: pointer;
  height: 2.3em;
  width: 3em;
  z-index: 150;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  @media (min-width: ${bp.m}) {
    display: none;
  }
`
const BurgerBar = styled.span`
  background-color: ${props => props.theme.colors.secondary};
  height: 0.4em;
  width: 100%;
  display: inline-block;
  transform-origin: center;
  transition-duration: 86ms;
  transition-property: transform;
  transition-timing-function: ease-out;

  ${Burger}.is-active &:nth-child(1) {
    transform: translateY(0.98em) rotate(45deg);
  }
  ${Burger}.is-active &:nth-child(2) {
    opacity: 0;
  }
  ${Burger}.is-active &:nth-child(3) {
    transform: translateY(-0.98em) rotate(-45deg);
  }
`
const NavBarStyled = styled(animated.nav)`
  grid-area: navbar;
  height: 3.7em;
  font-size: 20px;
  z-index: 100;
  justify-content: space-between;
  flex-direction: row;
  display: flex;
  flex-shrink: 0;
  position: sticky;
  top: ${props => props.top};
  box-shadow: ${props => props.theme.hardShadow };
  /* If we're coming from outside in dark mode -> start dark */
  .dark &{
    background: ${DarkTheme.colors.background}!important;
  }
  @media (min-width: ${bp.m}) {
    transition: background-color 0.5s ease-out;
    box-shadow: ${props => props.theme.hardShadow }!important;
    display: flex;
    flex-direction: column-reverse;
    transform: none;
    background-color: ${props => props.theme.colors.background}!important;
    position: sticky;
    top: 0!important;
    height: 100vh;
  }
`
const NavMenu = styled(animated.div)`
  font-size: 3em;
  width: 100%;
  position: absolute;
  transform-origin: 0% 0%;
  transform: translate(0, -100%);
  text-align: center;
  background-color: ${props => props.theme.colors.background};
  transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0), background-color 0.5s ease-out;
  /* If we're coming from outside in dark mode -> start dark */
  .dark &{
    background: ${DarkTheme.colors.background}!important;
  }
  &.is-active {
    transform: none;
  }
  @media (min-width: ${bp.m}) {
    position: initial;
    transform: none;      
    flex-grow: 1;
    flex-shrink: 0;
    flex-direction: column;
    display: flex;
    & > div {
      align-items: center;
      display: flex;
      margin-left: auto;
    }
  }
  @media (min-width: ${bp.l}) {
  }

`
const NavBarItem = styled(Link)`
  cursor: pointer;
  display: block;
  flex-grow: 0;
  flex-shrink: 0;
  display: block;
  line-height: 1.5;
  position: relative;
`

const Navbar = React.memo(({children}) => {
  // This is when the menu is active. The user is navigating the menu.
  const [menuOpened, setMenuOpened] = useState(false);
  // This is when the menu is retracted. The user is scrolling down.
  const [menuRetracted, setMenuRetracted] = useState(false);
  // This is when the menu is transparent. We're not scrolled to the top of the page.
  const [menuTransparent, setMenuTransparent] = useState(false);

  const prevScrollPosRef = useRef(0);

  const theme = useContext(ThemeContext);

  const listenScrollEvent = () => {
    const prevScrollPos = prevScrollPosRef.current;
    setMenuTransparent(window.scrollY > 0 ? true : false);
    setMenuRetracted(prevScrollPos > window.pageYOffset ? false : true);
    setMenuOpened(prevScrollPos > window.pageYOffset ? menuOpened : false)
    prevScrollPosRef.current = window.pageYOffset;
  }

  useEffect(() => {
    window.addEventListener('scroll', debounce(listenScrollEvent, 100))
    return () => window.removeEventListener('scroll', debounce(listenScrollEvent, 5))
  }, [])

  const animationRetracted = useSpring({
    to : { top : menuRetracted ? '-3.7em' : '0em' },
    config: { mass: 1, tension: 210, friction: 20, clamp: true }
  })

  const animationTransparent = useSpring({
    to : { boxShadow : menuTransparent ? '0 0px 0px rgba(0,0,0,0), 0 0px 0px rgba(0,0,0,0)' : theme.hardShadow, backgroundColor : menuTransparent ? theme.colors.backgroundTransparent : theme.colors.background },
    config: { mass: 1, tension: 210, friction: 20, clamp: true }
  })

    return (
      <NavBarStyled
        role="navigation"
        aria-label="main-navigation"
        style={{...animationRetracted, ...animationTransparent}}
      >
        { children }
          <Burger className={menuOpened ? 'is-active' : ''} onClick={() => setMenuOpened(!menuOpened)}>
              <BurgerBar />
              <BurgerBar />
              <BurgerBar />
          </Burger>
          <NavMenu className={menuOpened ? 'is-active' : ''} onClick={() => setMenuOpened(!menuOpened)}>
              <NavBarItem activeStyle={{ "textDecoration": "underline", "textDecorationColor": theme.colors.secondary}} to="/">
                Home
              </NavBarItem>
              <NavBarItem activeStyle={{ "textDecoration": "underline", "textDecorationColor": theme.colors.secondary }} partiallyActive={true} to="/posts">
                Posts
              </NavBarItem>
              <NavBarItem activeStyle={{ "textDecoration": "underline", "textDecorationColor": theme.colors.secondary }} partiallyActive={true} to="/stuff">
                Stuff
              </NavBarItem>
              <NavBarItem activeStyle={{ "textDecoration": "underline", "textDecorationColor": theme.colors.secondary }} to="/about">
                About
              </NavBarItem>
          </NavMenu>
      </NavBarStyled>
    )
})

export default Navbar