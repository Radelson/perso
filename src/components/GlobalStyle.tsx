import { createGlobalStyle } from "styled-components"
import Breakpoints from "../breakpoints"
import CrimsonPro from "../fonts/CrimsonPro-Roman-VF.ttf"
import MiniPax from "../fonts/Minipax-Variable.ttf"
import { DarkTheme } from "./Theme"



export default createGlobalStyle`
  body {
    margin: 0px;
    height: 100%;
    transition: background-color 0.5s ease-out;
    background-color: ${props => props.theme.colors.background};
  }
  body.dark {
    transition: none;
    background-color: ${DarkTheme.colors.background};
  }

  html {
    @font-face {
      font-family: 'CrimsonPro';
      src: url(${CrimsonPro}) format('truetype');
    }
    @font-face {
      font-family: 'MiniPax';
      src: url(${MiniPax}) format('truetype');
    }
    font-family: 'CrimsonPro', sans-serif;
    font-size: 23px;
    height: 100%;
    color: ${props => props.theme.colors.text};
    @media screen and (min-width: ${Breakpoints.m}) {
      font-size: 25px;
    } 
    @media screen and (min-width: ${Breakpoints.l}) {
      font-size: 26px;
    } 
    @media screen and (min-width: ${Breakpoints.xl}) {
      font-size: 32px;
    }
  }

  h2, h3, h4, h5, h6 {
    font-family: 'MiniPax', sans-serif;
    font-weight: 268;
  }

  h1 {
    font-size: 62px;
    font-family: 'CrimsonPro', sans-serif;
  }

  h2 {
    line-height: 1em;
    font-size: 42px;
  }

  a {
    text-decoration: none;
    color: ${props => props.theme.colors.text};
  }

  #___gatsby {
    height: 100%;
  }

  #gatsby-focus-wrapper {
    height: 100%;
  }

  div[role="group"][tabindex] {
    height: 100%;
  }

  pre {
    overflow-x: scroll;
  }
`
