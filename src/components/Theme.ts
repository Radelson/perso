import { DefaultTheme } from 'styled-components'

export const LightTheme: DefaultTheme = {
  borderRadius: '5px',

  altFont: 'Courier New',

  lightShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
  hardShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
  subtleShadow: `0 0.6px 2.7px -7px rgba(0, 0, 0, 0.046),
  0 1.5px 6.9px -7px rgba(0, 0, 0, 0.06),
  0 3px 14.2px -7px rgba(0, 0, 0, 0.062),
  0 6.2px 29.2px -7px rgba(0, 0, 0, 0.06),
  0 17px 80px -7px rgba(0, 0, 0, 0.07)`,

  colors: {
    main: 'hsl(37, 71%, 100%, 1)',
    secondary: 'hsl(37, 100%, 65%, 1)',
    
    secondaryTextColor: 'black',
    muted: 'hsl(38, 100%, 90%)', 
    accent: 'hsl(330, 65%, 76%)',
    plsCome: 'hsl(8, 61%, 90%)',
    andNowForSomethingElse: 'hsl(160, 68%, 11%)',
    bleed: 'hsla(38, 100%, 90%, 0.2)',

    text: 'hsl(0, 0%, 0%)',
    background: 'hsla(0, 0%, 100%, 1)',
    backgroundTransparent: 'hsla(0, 0%, 100%, 0)'
  },
}

export const DarkTheme: DefaultTheme = {
  borderRadius: '5px',

  altFont: 'Courier New',

  lightShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
  hardShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
  subtleShadow: `0 0.6px 2.7px -7px rgba(0, 0, 0, 0.046),
  0 1.5px 6.9px -7px rgba(0, 0, 0, 0.06),
  0 3px 14.2px -7px rgba(0, 0, 0, 0.062),
  0 6.2px 29.2px -7px rgba(0, 0, 0, 0.06),
  0 17px 80px -7px rgba(0, 0, 0, 0.07)`,

  colors: {
    main: 'hsl(1, 100%, 73%)',
    secondary: 'hsl(330, 65%, 76%)',

    muted: 'hsl(222, 60%, 88%)',
    accent: 'hsl(330, 65%, 76%)',
    plsCome: 'hsl(8, 61%, 90%)',
    secondaryTextColor: 'hsla(211, 32%, 35%, 1)',

    andNowForSomethingElse: 'hsl(160, 68%, 11%)',
    bleed: 'hsla(222, 60%, 20%, 0.1)',

    text: 'hsl(0, 0%, 100%, 0.8)',
    background: 'hsla(211, 32%, 35%, 1)',
    backgroundTransparent: 'hsla(211, 32%, 35%, 0)'
  },
}



