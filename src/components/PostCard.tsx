import React, { ReactNode, useEffect } from "react"
import { Link } from "gatsby"
import Img from "gatsby-image"
import styled from "styled-components"
import { MarkdownRemarkEdge } from "graphqlTypes"
import bp from "../breakpoints"
import { useInView } from 'react-intersection-observer'
import { useSpring, animated, config } from 'react-spring'

//Can't type PostCard with SFC generic because of this : https://github.com/DefinitelyTyped/DefinitelyTyped/issues/34237
interface PostCardProps {
  post: MarkdownRemarkEdge
}

const StyledImg = styled(Img)`
    grid-area: img;

`
const CardLink = animated(styled(Link)`
	box-shadow: ${props => props.theme.lightShadow};
  margin-bottom: 1em;
  min-height: 555px;
  width: 100%;
  display: grid;
  grid-template-rows: 400px 1.5em auto 1.5em;
  grid-template-columns: 1em 1fr 1em;
  grid-template-areas:
      "img img img"
      "metadata metadata metadata"
      ". desc ."
      ". . .";
  &:hover {
  }
  @media (min-width: ${bp.sm}) {
    width: 555px;
    margin-bottom: 2em;
  }

  @media (min-width: ${bp.m}) {
    width: 800px;
    grid-template-rows: auto 1.5em auto;
    grid-template-columns: 1fr 1em 1fr 1em;
    grid-template-areas:
      "img . title ."
      "img metadata metadata metadata"
      "img . desc .";
  }

  @media (min-width: ${bp.l}) {
    width: 1200px;
  }

  @media (min-width: ${bp.xl}) {
    width: 1400px;
  }
`)

const Metadata = styled(animated.div)`
  display: flex;
  align-items: center;
  grid-area: metadata;
  justify-content: space-evenly;
  color: ${props => props.theme.colors.secondaryTextColor};
  line-height: 0.7em;
  font-size: 0.8em;
  background-color: ${props => props.theme.colors.muted};
  font-family: ${props => props.theme.altFont}, monospace;

`
const Date = styled.span`
`
const Tags = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: start;
  width: 50%;
  `
const Tag = styled.span`
  margin-right: auto;
  font-weight: 100;
`
const Title = styled.h2`
  color: white;
  grid-area: img;
  align-self: end;
  z-index: 1;
  padding: 0.5em;
  background-color: ${props => props.theme.colors.bleed};
  @media (min-width: ${bp.m}) {
    grid-area: title;
    font-size: 2em;
    color: inherit;
    background-color: inherit;
  }
`

const Description = styled.div`
  grid-area: desc;
  padding: 1em 0 1em 0;
  font-size: 1.2em;
`

const calc = (x, y) => [-(y - window.innerHeight / 2) / 500, (x - window.innerWidth / 2) / 500, 0.999]
const trans = (x, y, s) => `perspective(2000px) rotateX(${x}deg) rotateY(${y}deg) scale(${s})`

const PostCard = ({ post }): ReactNode => {
  const [ref, inView] = useInView({
    rootMargin: '-100px 0px',
    triggerOnce: true,
  });
  
  const [Animation, set] = useSpring(() => ({
    opacity: inView ? 1 : 0,
    xys: [0, 0, 1]
  }));

  useEffect(() => {
    if(inView) {
      set({opacity: 1});
    }
  }, [inView]);

   return (
   <CardLink
    ref={ref} 
    onMouseMove={({ clientX: x, clientY: y }) => set({ xys: calc(x, y) })}
    onMouseLeave={() => set({ xys: [0, 0, 1] })}
    style={{ transform: Animation.xys.interpolate(trans), opacity: Animation.opacity}}
    to={post.node.fields.slug}>
    <StyledImg fluid={post.imgs}></StyledImg>
      <Title>{post.node.frontmatter.title}</Title>
      <Metadata>
        <Tags>{post.node.frontmatter.tags.map((tag) => <Tag key={tag}>{tag}</Tag>)}</Tags>
        <Date>{post.node.parent.modifiedTime}</Date>
      </Metadata>
      <Description>
        {post.node.frontmatter.description}
      </Description>
  </CardLink>
  );
}

export default PostCard;
