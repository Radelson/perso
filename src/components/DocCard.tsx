import React from 'react'
import styled from 'styled-components'
import bp from "../breakpoints"
import { Link } from "gatsby"

const NestedDocCardContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
`

const StyledLink = styled(Link)`
padding-left: 1em;
padding-right: 1em;
padding-bottom: 0.35em;
padding-top: 0.35em;
display: block;
box-shadow: ${props => props.theme.subtleShadow };
margin-bottom: 1em;
@media (min-width: ${bp.m}) {
    padding-top: 0em;
    padding-bottom: 0.35em;
  }
`

const StyledIntro = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const StyledDocTitle = styled.h3`
  margin-top: 0em;
  width: 100%;
  text-align: center;
  margin-bottom: 0em;
  @media (min-width: ${bp.m}) {
    margin-top: 1em;
    margin-bottom: 1em;
    text-align: initial;
  }
`

const StyledDocDesc = styled.p`

`
const Tags = styled.div`
  display: flex;
  font-size: 0.7em;
  align-items: center;
  margin-left: 1em;
  margin-right: auto;
  font-family: ${props => props.theme.altFont}, monospace;
  flex-wrap: wrap;
`
const Tag = styled.span`
  margin-right: auto;
  font-weight: 100;
  align-items: center;
  padding-right: 1em;
  padding-left: 1em;
  text-decoration: underline;
  @media (min-width: ${bp.m}) {
    &::before {
    content: "/"
  }
  }
`

const DocCardContainer = styled.div`
padding-left: 1em;
padding-right: 1em;
display: block;
margin-bottom: 1em;
width: 75%;
@media (min-width: ${bp.m}) {
  max-width: 71%;
}
@media (min-width: 1500px) {
  width: 1400px;
}
`
const NestedStyledLink = styled(Link)`
padding: 1em;
display: block;
box-shadow: ${props => props.theme.lightShadow };
margin-bottom: 1em;
text-align: center;
width: 90%;
@media (min-width: ${bp.m}) {
    width: 37%;

  }
`

const DocCard = ({doc}) => (
    <DocCardContainer>
      <StyledLink to={doc.fields.slug}>
        <StyledIntro>
          <StyledDocTitle>{doc.frontmatter.title}</StyledDocTitle>
          <Tags>{doc.frontmatter.tags.map((tag, index) => <Tag key={index}>{tag}</Tag>)}</Tags>
        </StyledIntro>
        <StyledDocDesc>{doc.frontmatter.description}</StyledDocDesc>
      </StyledLink>
      <NestedDocCardContainer>
  {doc.children.map((nestedDoc, index) => 
  <NestedStyledLink key={index} to={nestedDoc.fields.slug}>
    {nestedDoc.frontmatter.title}
  </NestedStyledLink>)}
      </NestedDocCardContainer>
    </DocCardContainer>
  )

export default DocCard