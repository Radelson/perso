import 'styled-components'

declare module 'styled-components' {
  export interface DefaultTheme {
    lightShadow: string
    hardShadow: string
    subtleShadow: string
    borderRadius: string
    altFont: string
    colors: {
      main: string
      secondary: string
      text: string
      muted: string
      accent: string
      plsCome: string
      andNowForSomethingElse: string
      bleed: string
      background: string
      secondaryTextColor: string
      backgroundTransparent: string
    }
  }
}