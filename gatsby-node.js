/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const { introspectionQuery, graphql, printSchema } = require("gatsby/graphql")
const write = require("write")
const path = require('path')
const { createFilePath } = require('gatsby-source-filesystem')


exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions

  return graphql(`
    {
      allMarkdownRemark(limit: 1000) {
        edges {
          node {
            id
            fields {
              slug
            }
            frontmatter {
              templateKey
            }
          }
        }
      }
    }
  `).then(result => {
    if (result.errors) {
      result.errors.forEach(e => console.error(e.toString()))
      return Promise.reject(result.errors)
    }
    const pages = result.data.allMarkdownRemark.edges
    pages.forEach(edge => {
      createPage({
        path: edge.node.fields.slug,
        component: path.resolve(
          `src/templates/${String(edge.node.frontmatter.templateKey)}.tsx`
        ),
        context: {
          slug: edge.node.fields.slug,
          pageType : edge.node.frontmatter.templateKey,
        },
      })
    })
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions
  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode})
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}

/**
 * Create parent-child link between wiki pages.
 * Inspired from here : https://github.com/gatsbyjs/gatsby/issues/3129#issuecomment-360927436
 */
exports.sourceNodes = ({ actions, getNodesByType }) => {
  const { createParentChildLink } = actions

  const markdownNodes = getNodesByType(`MarkdownRemark`)

    markdownNodes.forEach(node => {
        const childNodes = getNodesByType(`MarkdownRemark`).filter(
          nodeToCompare => {
            // Create a relative path between 2 dirs
            const relative = path.relative(node.fields.slug, nodeToCompare.fields.slug)
            // If the relative path has only 1 member (i.e. like this : path/ ), it's a child node
            return relative && !relative.startsWith('..') && !path.isAbsolute(relative) && relative.split('/').length < 2
          })
        childNodes.forEach(childNode => {
          createParentChildLink({parent: node, child: childNode})
        })
      }
    )
}

/**
 * Generate GraphQL schema.json file to be read by tslint
 * Thanks: https://gist.github.com/kkemple/6169e8dc16369b7c01ad7408fc7917a9
 */
exports.onPostBootstrap = async ({ store }) => {
  try {
    const { schema } = store.getState()
    const jsonSchema = await graphql(schema, introspectionQuery)
    const sdlSchema = printSchema(schema)

    write.sync("schema.json", JSON.stringify(jsonSchema.data), {})
    write.sync("schema.graphql", sdlSchema, {})

    console.log("\n\n[gatsby-plugin-extract-schema] Wrote schema\n") // eslint-disable-line
  } catch (error) {
    console.error(
      "\n\n[gatsby-plugin-extract-schema] Failed to write schema: ",
      error,
      "\n"
    )
  }
}