# Notes

## Typescript setup

### Setup eslint

> Ripped eveything off <https://github.com/erikdstock/gatsby-starter-ts-mdx>

### Setup all the packages needed (types, etc)

That was a chore as the is peer deps on multiple graphql versions
Eventually got it working deleting package.lock and node_modules

### Setup graphql types gen

Setup from erik's repo gave me eslint's config and code to check queries against our graphql schema. The next step is generating the types so I don't have to ponder on each of interfaces for the longest time
Got it from here : <https://www.cosmiccode.blog/blog/gatsby-typings-from-gql/>

### Start coding

## Ressources used

- <https://www.styled-components.com/docs/api#typescript>
- <https://www.typescriptlang.org/docs/handbook/functions.html>
- <https://www.pluralsight.com/guides/typescript-building-react-components>
- <https://www.pluralsight.com/guides/how-to-statically-type-react-components-with-typescript>
- <https://github.com/erikdstock/gatsby-starter-ts-mdx>
- <https://www.cosmiccode.blog/blog/gatsby-typings-from-gql/>
- <https://graphql-code-generator.com/docs/integrations/gatsby>
- <https://css-tricks.com/rem-global-em-local/>
- <https://responsivedesign.is/strategy/page-layout/defining-breakpoints/>
- <https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Realizing_common_layouts_using_CSS_Grid_Layout>


## Todo

### Next step

- Review effect of container on typography posts -> this looks too tiring to read
- Check if we can have images overflowing containers on posts/docs
- Finish static pages Content
  - CV + About page + 404 + Home page (Links) + Footer (Add Ali)
- Use hsla everywhere
- Setup react helmet
- Merge all typographic stuff in a nice component for docs and posts + whatever

### Before live

- Write at least 1 post + Start track on K3s ?
- Do a 404 page
- Review fonts flickers : https://github.com/styled-components/styled-components/issues/2227
- Homepages
  - Check if I can hide my mail/phone ?
- Check that shit : https://www.youtube.com/watch?v=w8pksaGhjfA
-- Text
- Probably swap the animations of the navbar from css to react-spring

### After live

- Check how to use the posts/docs internal AST to create navigations based on titles
- Create a "pro" page with CV
- Clean up code
  - Normalize every case
  - Normalize code style
  - Actually use Typescript
- Fix animation themeswitcher when clicked repeatedly

### After K3S

- Self host
- Self host comments
